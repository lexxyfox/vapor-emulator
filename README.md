# OwO wats dis??

This is my personal, clean-room developed Steam emulation layer. It allows executing programs that depend on the Steam API without the official Steam client software from Valve Software. This project is in no way affiliated with, endorsed by, or supported by Valve Software. 

Some goals of this project compared to others:
* Don't unnecessarily sanitize strings.
* Don't be a derp and mix char[], char*, cstring, std::string, et al. 
* Don't store config files on disk and instead use environment variables. Most programs are started by a shell script and/or XDG desktop file, both of which can set environment varibles. If a config file is desired, utilize `dotenv`. If global settings are desired, put them in your `~/.profile`. Please note that this doesn't preclude this emulator from working on ⩊ℹ︎∏∂º₩∫ - they have environment variables too!
* Don't use any code written by any current Steamworks Partners.
* Don't have circular #include directives
* Compile with no warnings (-Wall -Werror)
* Be smol and fast

# Environment Variables

| name | desc | 
| ---- | ---- |
| SteamAppId | AppID of the game. If not set, `./steam_appid.txt` is usually read. Is sometimes used to determine which channel of the game to load (e.g. BETA). | 
| SteamPersonaName | Set your persona name. This is the name that's displayed to other users. If not set, reads from $USER. If that is blank, defaults to `SteamUser`. |
| SteamDLC | A semicolon (;) separated list of (numerical) DLC ids. Optionally a DLC id may be followed by a space and the (string) name of the DLC (this is used by some games sometimes). The DLC name cannot include semicolons (sorry Japan!), and will automatically be truncated if required. All provided DLC are marked as both available and installed (might provide option in the future). Example: `SteamDLC='1918370;304212 Super Awesome DLC'` |