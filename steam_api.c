#include "steam_api_common.h"

#include <stdbool.h>
#include <stdio.h>

/** https://partner.steamgames.com/doc/api/steam_api#SteamAPI_RestartAppIfNecessary */
S_API bool SteamAPI_RestartAppIfNecessary (uint32 unOwnAppID) {
  INFO ("SteamAPI_RestartAppIfNecessary(%u) → false", unOwnAppID);
  return false;
}

/** https://partner.steamgames.com/doc/api/steam_api#SteamAPI_Init */
S_API bool SteamAPI_Init ( ) {
  INFO ("SteamAPI_Init() → true");
  return true;
}

// TODO
/** https://partner.steamgames.com/doc/api/steam_api#SteamAPI_RunCallbacks */
S_API void SteamAPI_RunCallbacks ( ) {
  INFO ("SteamAPI_RunCallbacks() → void");
}

/** https://partner.steamgames.com/doc/api/steam_api#SteamAPI_Shutdown */
S_API void SteamAPI_Shutdown ( ) { INFO ("SteamAPI_Shutdown() → void"); }