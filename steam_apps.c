#include "steam_apps.h"

#include "steam_api_common.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// TODO: support unicode
/** https://partner.steamgames.com/doc/api/ISteamApps#GetDLCCount */
S_API int SteamAPI_ISteamApps_GetDLCCount (SteamApps *this) {
  char *dlclist = getenv ("SteamDLC");
  unsigned int i = strlen (dlclist);

  if (!dlclist || i == 0) {
    INFO ("SteamAPI_ISteamApps_GetDLCCount(%p) → 0", this);
    return 0;
  }

  unsigned int c = 1;
  for (; --i;)
    if (dlclist[i] == ';')
      ++c;

  INFO ("SteamAPI_ISteamApps_GetDLCCount(%p) → %u", this, c);
  return c;
}

// TODO: support unicode
/** https://partner.steamgames.com/doc/api/ISteamApps#BGetDLCDataByIndex */
S_API bool SteamAPI_ISteamApps_BGetDLCDataByIndex (
    SteamApps *this,
    int iDLC,
    AppId_t *pAppID,
    bool *pbAvailable,
    char *pchName,
    int cchNameBufferSize
) {
  char *dlclist = getenv ("SteamDLC");

  if (!dlclist) {
    INFO (
        "SteamAPI_ISteamApps_BGetDLCDataByIndex(%p, %u, %p, %p, %p, %i) → "
        "false",
        this, iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize
    );
    return false;
  }

  int idx = 0;
  char chr = '\0';
  char *id_start = dlclist--;
  char *id_stop = NULL;

  do {
    chr = *++dlclist;
    if (chr == ' ' && id_stop == NULL)
      id_stop = dlclist;
    else if ((chr == ';') || (chr == '\0')) {
      if (idx++ < iDLC) {
        id_start = dlclist + 1;
        id_stop = NULL;
        continue;
      }

      *pAppID = strtoul (id_start, NULL, 10);
      *pbAvailable = true;

      if (id_stop == NULL) {
        id_stop = dlclist;
        pchName[0] = '\0';
      } else {
        int name_len = min (dlclist - id_stop - 1, cchNameBufferSize);
        pchName[name_len] = 0;
        memcpy (pchName, id_stop + 1, name_len);
      }

      INFO (
          "SteamAPI_ISteamApps_BGetDLCDataByIndex(%p, %u, %p, %p, %p, %i) → "
          "true",
          this, iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize
      );
      return true;
    }
  } while (chr != '\0');

  INFO (
      "SteamAPI_ISteamApps_BGetDLCDataByIndex(%p, %u, %p, %p, %p, %i) → false",
      this, iDLC, pAppID, pbAvailable, pchName, cchNameBufferSize
  );
  return false;
}

S_API bool
SteamAPI_ISteamApps_BIsDlcInstalled (SteamApps *this, AppId_t appID) {
  INFO ("SteamAPI_ISteamApps_BIsDlcInstalled(%p, %u) → true", this, appID);
  return true;
}