#include "steam_remotestorage.h"
#include "steam_api_common.h"
#include <stdbool.h>

// TODO: make ENV option to set this
/** https://partner.steamgames.com/doc/api/ISteamRemoteStorage#IsCloudEnabledForAccount */
S_API bool SteamAPI_ISteamRemoteStorage_IsCloudEnabledForAccount(
    SteamRemoteStorage *this) {
  INFO("SteamAPI_ISteamRemoteStorage_IsCloudEnabledForAccount(%p) → true", this);
  return true;
}

// TODO: make ENV option to set this
/** https://partner.steamgames.com/doc/api/ISteamRemoteStorage#IsCloudEnabledForApp */
S_API bool SteamAPI_ISteamRemoteStorage_IsCloudEnabledForApp(
    SteamRemoteStorage *this) {
  INFO("SteamAPI_ISteamRemoteStorage_IsCloudEnabledForApp(%p) → true", this);
  return true;
}