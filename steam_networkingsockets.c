#include "steam_networkingsockets.h"
#include "steam_api_common.h"
#include "steam_networkingtypes.h"

/** https://partner.steamgames.com/doc/api/ISteamNetworkingSockets#InitAuthentication */
S_API ESteamNetworkingAvailability
SteamAPI_ISteamNetworkingSockets_InitAuthentication(
    SteamNetworkingSockets *this) {
  INFO("SteamAPI_ISteamNetworkingSockets_InitAuthentication() → %i",
       k_ESteamNetworkingAvailability_Current);
  return k_ESteamNetworkingAvailability_Current;
}