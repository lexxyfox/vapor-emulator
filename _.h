#include <stdio.h>

#ifdef DEBUG
#define INFO(s, ...)                                                          \
  printf ("\x1b[1m\x1b[32m\xe2\x96\xb6 " s "\x1b[0m\n", ##__VA_ARGS__);       \
  fflush (stdout);
#define ERR(s, ...)                                                           \
  printf ("\x1b[1m\x1b[31m■ " s "\x1b[0m\n", ##__VA_ARGS__);                  \
  fflush (stdout);
#else
#define INFO(s, ...)
#define ERR(s, ...)
#endif

#define max(a, b)                                                             \
  ({                                                                          \
    __typeof__ (a) _a = (a);                                                  \
    __typeof__ (b) _b = (b);                                                  \
    _a > _b ? _a : _b;                                                        \
  })

#define min(a, b)                                                             \
  ({                                                                          \
    __typeof__ (a) _a = (a);                                                  \
    __typeof__ (b) _b = (b);                                                  \
    _a < _b ? _a : _b;                                                        \
  })