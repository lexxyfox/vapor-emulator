#include "steam_friends.h"
#include "steam_api_common.h"
#include <stdlib.h>

S_API const char *SteamAPI_ISteamFriends_GetPersonaName(SteamFriends *this) {
  char *name = getenv("SteamPersonaName");
  if (!name) {
    name = getenv("USER");
    if (!name)
      name = (char *)"SteamUser";
  }
  INFO("SteamAPI_ISteamFriends_GetPersonaName(%p) → \"%s\"", this, name);
  return name;
}