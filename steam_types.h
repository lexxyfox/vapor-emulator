#define S_CALLTYPE __attribute__((cdecl))

typedef unsigned char uint8;
typedef signed char int8;
typedef int int32;
typedef unsigned int uint32;

typedef uint32 AppId_t;