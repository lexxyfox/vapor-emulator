CFLAGS += -fdata-sections -ffunction-sections -fno-ident -fPIC -fshort-enums -O3 -Wall -Werror 
LDFLAGS += --gc-sections --hash-style=gnu --relax --shared --stats --strip-all -O3 -z noseparate-code -z norelro 
LDLIBS += -lc

SOURCES += \
	steam_api \
	steam_api_internal \
	steam_apps \
	steam_client \
	steam_friends \
	steam_networkingsockets \
	steam_remotestorage \

libsteam_api.so: $(addsuffix .o,$(SOURCES))
	$(LD) $(OUTPUT_OPTION) $(LDFLAGS) $(LDLIBS) $^

%.o: %.c

%.o: %.s
	$(AS) $(OUTPUT_OPTION) $(ASFLAGS) '$<'

%.s: %.i
	$(CC) $(OUTPUT_OPTION) $(CFLAGS) -S '$<' 

%.i: %.c 
	$(CPP) $(OUTPUT_OPTION) $(CPPFLAGS) '$<'

.PHONY: clean

clean:
	$(RM) -r *.i *.s *.o *.so