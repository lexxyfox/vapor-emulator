#include "steam_api_common.h"
#include "steam_client.h"
#include "steam_networkingsockets.h"
#include "steam_networkingutils.h"

#include <stdlib.h>
#include <string.h>

S_API HSteamUser *SteamAPI_GetHSteamUser ( ) {
  HSteamUser *user = malloc (sizeof *user);
  INFO ("SteamAPI_GetHSteamPipe() → %p", user);
  return user;
}

S_API HSteamPipe *SteamAPI_GetHSteamPipe ( ) {
  HSteamPipe *pipe = malloc (sizeof *pipe);
  INFO ("SteamAPI_GetHSteamPipe() → %p", pipe);
  return pipe;
}

S_API SteamClient *SteamInternal_CreateInterface ( ) {
  SteamClient *client = malloc (sizeof *client);
  INFO ("SteamInternal_CreateInterface() → %p", client);
  return client;
}

S_API void *SteamInternal_FindOrCreateUserInterface (
    HSteamUser *hSteamUser, const char *pszVersion
) {
  void *ret = NULL;
  if (strncmp ("SteamNetworkingUtils", pszVersion, 20) == 0) {
    ret = malloc (sizeof (SteamNetworkingUtils));
  } else if (strncmp ("SteamNetworkingSockets", pszVersion, 22) == 0) {
    ret = malloc (sizeof (SteamNetworkingSockets));
  } else {
    ERR (
        "SteamInternal_FindOrCreateUserInterface(%p, \"%s\") → %p", hSteamUser,
        pszVersion, ret
    );
    ERR ("\tUnknown interface version!");
    return NULL;
  }
  INFO (
      "SteamInternal_FindOrCreateUserInterface(%p, \"%s\") → %p", hSteamUser,
      pszVersion, ret
  );
  return ret;
}

S_API void SteamAPI_RegisterCallback (CCallbackBase *pCallback, int iCallback) {
  INFO ("SteamAPI_RegisterCallback(%p, %i) → void", pCallback, iCallback);
  INFO (
      "\t{vptr: %p, CallbackFlags: %i, Callback: %i}", pCallback->vptr,
      pCallback->m_nCallbackFlags, pCallback->m_iCallback
  );
}