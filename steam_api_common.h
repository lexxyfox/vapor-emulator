#include "_.h"
#include "stdint.h"
#include "steam_types.h"

#define S_API __attribute__((used))

typedef struct HSteamUser {

} HSteamUser;

typedef struct HSteamPipe {

} HSteamPipe;

typedef void *SteamAPIWarningMessageHook_t(int, const char *);

typedef struct CCallbackBase {
  void *vptr;
  uint8 m_nCallbackFlags;
  int m_iCallback;
} CCallbackBase;