#include "steam_client.h"
#include "steam_api_common.h"
#include "steam_applist.h"
#include "steam_apps.h"
#include "steam_controller.h"
#include "steam_friends.h"
#include "steam_gamesearch.h"
#include "steam_htmlsurface.h"
#include "steam_http.h"
#include "steam_input.h"
#include "steam_inventory.h"
#include "steam_matchmaking.h"
#include "steam_music.h"
#include "steam_musicremote.h"
#include "steam_networking.h"
#include "steam_parentalsettings.h"
#include "steam_parties.h"
#include "steam_remoteplay.h"
#include "steam_remotestorage.h"
#include "steam_screenshots.h"
#include "steam_ugc.h"
#include "steam_user.h"
#include "steam_userstats.h"
#include "steam_utils.h"
#include "steam_video.h"
#include <stdlib.h>

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamUser */
S_API SteamUser *SteamAPI_ISteamClient_GetISteamUser(SteamClient *this,
                                                     HSteamUser *hSteamUser,
                                                     HSteamPipe *hSteamPipe,
                                                     const char *pchVersion) {
  SteamUser *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamUser(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamFriends */
S_API SteamFriends *SteamAPI_ISteamClient_GetISteamFriends(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamFriends *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamFriends(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamUtils */
S_API SteamUtils *SteamAPI_ISteamClient_GetISteamUtils(SteamClient *this,
                                                       HSteamPipe *hSteamPipe,
                                                       const char *pchVersion) {
  SteamUtils *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamUtils(%p, %p, \"%s\") → %p", this,
       hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamMatchmaking */
S_API SteamMatchmaking *SteamAPI_ISteamClient_GetISteamMatchmaking(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamMatchmaking *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamMatchmaking(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamMatchmakingServers */
S_API SteamMatchmakingServers *
SteamAPI_ISteamClient_GetISteamMatchmakingServers(SteamClient *this,
                                                  HSteamUser *hSteamUser,
                                                  HSteamPipe *hSteamPipe,
                                                  const char *pchVersion) {
  SteamMatchmakingServers *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamMatchmaking(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamUserStats */
S_API SteamUserStats *SteamAPI_ISteamClient_GetISteamUserStats(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamUserStats *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamUserStats(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamApps */
S_API SteamApps *SteamAPI_ISteamClient_GetISteamApps(SteamClient *this,
                                                     HSteamUser *hSteamUser,
                                                     HSteamPipe *hSteamPipe,
                                                     const char *pchVersion) {
  SteamApps *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamApps(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamNetworking */
S_API SteamNetworking *SteamAPI_ISteamClient_GetISteamNetworking(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamNetworking *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamNetworking(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamRemoteStorage
 */
S_API SteamRemoteStorage *
SteamAPI_ISteamClient_GetISteamRemoteStorage(SteamClient *this,
                                             HSteamUser *hSteamUser,
                                             HSteamPipe *hSteamPipe,
                                             const char *pchVersion) {
  SteamRemoteStorage *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamRemoteStorage(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamScreenshots */
S_API SteamScreenshots *SteamAPI_ISteamClient_GetISteamScreenshots(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamScreenshots *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamScreenshots(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamGameSearch */
S_API SteamGameSearch *SteamAPI_ISteamClient_GetISteamGameSearch(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamGameSearch *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamGameSearch(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamHTTP */
S_API SteamHTTP *
SteamAPI_ISteamClient_GetISteamHTTP(SteamClient *this, HSteamUser *hSteamUser,
                                    HSteamPipe *hSteamPipe,
                                    const char *pchVersion) {
  SteamHTTP *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamHTTP(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamController */
S_API SteamController *SteamAPI_ISteamClient_GetISteamController(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamController *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamController(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamUGC */
S_API SteamUGC  *
SteamAPI_ISteamClient_GetISteamUGC(SteamClient *this, HSteamUser *hSteamUser,
                                   HSteamPipe *hSteamPipe,
                                   const char *pchVersion) {
  SteamUGC *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamUGC(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamAppList */
S_API SteamAppList *SteamAPI_ISteamClient_GetISteamAppList(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamAppList *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamAppList(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamMusic */
S_API SteamMusic *SteamAPI_ISteamClient_GetISteamMusic(SteamClient *this,
                                                       HSteamUser *hSteamUser,
                                                       HSteamPipe *hSteamPipe,
                                                       const char *pchVersion) {
  SteamMusic *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamMusic(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamMusicRemote */
S_API SteamMusicRemote *SteamAPI_ISteamClient_GetISteamMusicRemote(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamMusicRemote *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamMusicRemote(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamHTMLSurface */
S_API SteamHTMLSurface *SteamAPI_ISteamClient_GetISteamHTMLSurface(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamHTMLSurface *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamHTMLSurface(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamInventory */
S_API SteamInventory *SteamAPI_ISteamClient_GetISteamInventory(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamInventory *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamInventory(%p, %p, %p, \"%s\") → %p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamVideo */
S_API SteamVideo *SteamAPI_ISteamClient_GetISteamVideo(SteamClient *this,
                                                       HSteamUser *hSteamUser,
                                                       HSteamPipe *hSteamPipe,
                                                       const char *pchVersion) {
  SteamVideo *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamVideo(%p, %p, %p, \"%s\") → %p", this,
       hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamParentalSettings */
S_API SteamParentalSettings *SteamAPI_ISteamClient_GetISteamParentalSettings(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamParentalSettings *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamParentalSettings(%p, %p, %p, \"%s\") → "
       "%p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamInput */
S_API SteamInput *SteamAPI_ISteamClient_GetISteamInput(SteamClient *this,
                                                       HSteamUser *hSteamUser,
                                                       HSteamPipe *hSteamPipe,
                                                       const char *pchVersion) {
  SteamInput *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamInput(%p, %p, %p, \"%s\") → "
       "%p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamParties */
S_API SteamParties *SteamAPI_ISteamClient_GetISteamParties(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamParties *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamParties(%p, %p, %p, \"%s\") → "
       "%p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamRemotePlay */
S_API SteamRemotePlay *SteamAPI_ISteamClient_GetISteamRemotePlay(
    SteamClient *this, HSteamUser *hSteamUser, HSteamPipe *hSteamPipe,
    const char *pchVersion) {
  SteamRemotePlay *interface = malloc(sizeof *interface);
  INFO("SteamAPI_ISteamClient_GetISteamRemotePlay(%p, %p, %p, \"%s\") → "
       "%p",
       this, hSteamUser, hSteamPipe, pchVersion, interface);
  return interface;
}

/** https://partner.steamgames.com/doc/api/ISteamClient#GetISteamRemotePlay#SetWarningMessageHook */
S_API void SteamAPI_ISteamClient_SetWarningMessageHook(
    SteamClient *this, SteamAPIWarningMessageHook_t pFunction) {
  INFO("SteamAPI_ISteamClient_SetWarningMessageHook(%p, %p) -> void", this,
       pFunction);
}